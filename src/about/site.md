---
icon: info
article: false
breadcrumb: false
---

# 关于网站

网站使用 [VuePress](https://v1.vuepress.vuejs.org/zh/guide/) 建站工具构建，使用主题 [vuepress-theme-hope](https://github.com/Mister-Hope/vuepress-theme-hope/)

## 网站时间轴

* 2020-10-03 开始运营
* 2021-10-03 博客一周年纪念
* 2022-03-12 网站开始迁移
* 2022-03-15 网站迁移完成

## 免责声明

此博客包含 [阮一峰](https://github.com/ruanyf) 和 [廖雪峰](https://weibo.com/liaoxuefeng) 两位老师的博客和书籍。

在这里给他们致以诚挚的感谢。
