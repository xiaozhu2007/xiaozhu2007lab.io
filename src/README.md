---
home: true
layout: Blog
icon: home
title: 主页
heroImage: /logo.png
heroText: HackPig520 的博客
heroFullScreen: true
tagline: 小意思
projects:
  - name: QBot
    desc: 一个用Mineflayer做的Minecraft机器人✨
    link: https://gitee.com/xiaozhu2007/qbot
    icon: /assets/img/vuepress-hope-logo.svg

  - name: XBlog
    desc: 新一代更轻量的免数据库博客——XBlog
    link: https://gitee.com/xiaozhu2022/xblog
    icon: /assets/img/vuepress-hope-logo.svg

  - name: 代码教程
    icon: book
    link: /code/

  - name: 随笔
    icon: article
    link: /note/

footer: '<a href="https://icp.gov.moe/?keyword=20229899" target="_blank">萌ICP备20229899号</a> | <a href="/about/site.html">关于网站</a>'
---
